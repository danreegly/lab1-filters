﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Filters
{
    class ErosionFilter : Filters
    {
        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y)
        {
            int radiusX = morphCore.GetLength(0) / 2;
            int radiusY = morphCore.GetLength(1) / 2;
            float resultR = 255;
            float resultG = 255;
            float resultB = 255;
            for (int l = -radiusY; l <= radiusY; l++)
                for (int k = -radiusX; k <= radiusX; k++)
                {
                    int idx = Clamp(x + k, 0, sourceImage.Width - 1);
                    int idy = Clamp(l + y, 0, sourceImage.Height - 1);
                    Color neighborColor = sourceImage.GetPixel(idx, idy);
                    if ((morphCore[k + 1, l + 1]) && (neighborColor.R < resultR))
                        resultR = neighborColor.R;
                    if ((morphCore[k + 1, l + 1]) && (neighborColor.G < resultG))
                        resultG = neighborColor.G;
                    if (morphCore[k + 1, l + 1] && (neighborColor.B < resultB))
                        resultB = neighborColor.B;
                }
            return Color.FromArgb(
                Clamp((int)resultR, 0, 255),
                Clamp((int)resultG, 0, 255),
                Clamp((int)resultB, 0, 255)
                );
        }

    }
}
