﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filters
{
    class BorderFilter : MatrixFilter
    {
        public BorderFilter()
        {
            int sizeX = 3;
            int sizeY = 3;
            kernel = new float[sizeX, sizeY];
            for (int i = 0; i < sizeX; i++)
                for (int j = 0; j < sizeY; j++)
                {
                    float k = 0;
                    switch (j)
                    {
                        case 0:
                            k = -1;
                            break;
                        case 1:
                            k = 0;
                            break;
                        case 2:
                            k = 1;
                            break;
                    }
                    kernel[i, j] = k;
                }
        }
    }
}
