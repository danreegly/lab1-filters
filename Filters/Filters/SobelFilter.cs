﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filters
{
    class SobelFilter:MatrixFilter
    {
        public SobelFilter() 
        {
            createSobelKernel(3, 3); 
        }
        public void createSobelKernel(int radius, float sigma)
        {
            int size = 3;
            kernel = new float[size, size];
            for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                {
                    float k = 0;
                    switch (j)
                    {
                        case 0:
                            k = -1;
                            break;
                        case 1:
                            k = 0;
                            break;
                        case 2:
                            k = 1;
                            break;
                    }
                    if (i == 0 || i == 2)
                        kernel[i, j] = k;
                    else
                        kernel[i, j] = 2 * k;
                }
        }
    }
}
