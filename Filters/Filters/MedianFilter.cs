﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Filters
{
    class MedianFilter : Filters
    {
        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y)
        {
            int idX = 0, idY = 0;
            int sizeX = 3;
            int sizeY = 3;
            int[] kernelR = new int[sizeX * sizeY];
            int[] kernelG = new int[sizeX * sizeY];
            int[] kernelB = new int[sizeX * sizeY];
            for (int i = -sizeX / 2, k = 0; i <= sizeX / 2; i++)
                for (int j = -sizeY / 2; j <= sizeY / 2; j++, k++)
                {
                    idX = Clamp(x + i, 0, sourceImage.Width - 1);
                    idY = Clamp(y + j, 0, sourceImage.Height - 1);
                    kernelR[k] = sourceImage.GetPixel(idX, idY).R;
                    kernelG[k] = sourceImage.GetPixel(idX, idY).G;
                    kernelB[k] = sourceImage.GetPixel(idX, idY).B;
                }

            Array.Sort(kernelR);
            Array.Sort(kernelG);
            Array.Sort(kernelB);
            int median = sizeX * sizeY / 2;
            return Color.FromArgb(kernelR[median], kernelG[median], kernelB[median]);
        }
    }
}
