﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Filters
{
    class GradFilter : Filters
    {
        protected override Color calculateNewPixelColor(Bitmap sourceImage, int x, int y)
        {
            int radiusX = morphCore.GetLength(0) / 2;
            int radiusY = morphCore.GetLength(1) / 2;
            float DelR = 0;
            float DelG = 0;
            float DelB = 0;
            float EroR = 255;
            float EroG = 255;
            float EroB = 255;
            for (int l = -radiusY; l <= radiusY; l++)
                for (int k = -radiusX; k <= radiusX; k++)
                {
                    int idx = Clamp(x + k, 0, sourceImage.Width - 1);
                    int idy = Clamp(l + y, 0, sourceImage.Height - 1);
                    Color neighborColor = sourceImage.GetPixel(idx, idy);
                    if ((morphCore[k + 1, l + 1]) && (neighborColor.R > DelR))
                        DelR = neighborColor.R;
                    if ((morphCore[k + 1, l + 1]) && (neighborColor.R < EroR))
                        EroR = neighborColor.R;
                    if ((morphCore[k + 1, l + 1]) && (neighborColor.G > DelG))
                        DelG = neighborColor.G;
                    if ((morphCore[k + 1, l + 1]) && (neighborColor.G < EroG))
                        EroG = neighborColor.G;
                    if (morphCore[k + 1, l + 1] && (neighborColor.B > DelB))
                        DelB = neighborColor.B;
                    if (morphCore[k + 1, l + 1] && (neighborColor.B < EroB))
                        EroB = neighborColor.B;
                }
            return Color.FromArgb(
                Clamp((int)(DelR - EroR), 0, 255),
                Clamp((int)(DelG - EroG), 0, 255),
                Clamp((int)(DelB - EroB), 0, 255)
                );
        }

    }
}
